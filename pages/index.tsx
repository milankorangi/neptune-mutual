import type { NextPage } from "next";
import Head from "next/head";
import Image from "next/image";
import React, { useState } from "react";
import Connected from "../components/Connected";
import Disconnect from "../components/Disconnect";
import Web3 from "web3";
import Loading from "../components/Loading";

const Home: NextPage = () => {
    const [show, setShow] = useState<any>(false);
    const [connecting, setIsConnecting] = useState<any>(false);
    const [isConnected, setIsConnected] = useState<boolean>(false);
    const [currentAccount, setCurrentAccount] = useState<any>(null);
    const [chainId, setChainId] = useState<any>(null);
    const [balance, setBalance] = useState<any>(0);
    const [nep, setNep] = useState<any>("");
    const [busd, setBusd] = useState<any>("");

    const connect = async (provider: any) => {
        const web3 = new Web3(provider);
        const accounts = await web3.eth.getAccounts();
        const chain_id = await web3.eth.getChainId();
        if (accounts.length === 0) {
            console.log("Please connect to MetaMask!");
        } else if (accounts[0] !== currentAccount) {
            setChainId(chain_id)
            setCurrentAccount(accounts[0]);
            const accBalanceEth = web3.utils.fromWei(
                await web3.eth.getBalance(accounts[0]),
                "ether"
            );

            setBalance(Number(accBalanceEth).toFixed(2));
            setIsConnected(true);
            setIsConnecting(false);
        }
    };

    const handleNep = (e: any) => {
        setNep(e.target.value);
        if (e.target.value == "") {
            setBusd("");
        } else if (e.target.value) {
            setBusd(parseFloat((e.target.value * 3).toString()).toFixed(2));
        }
    };

    const handleBusd = (e: any) => {
        setBusd(e.target.value);
        if (e.target.value == "") {
            setNep("");
        } else if (e.target.value) {
            setNep(parseFloat((e.target.value / 3).toString()).toFixed(2));
        }
    };

    return (
        <>
            <Head>
                <title>Crypto Convertor</title>
                <link rel="icon" href="/fav.png" />
            </Head>
            <div className="flex flex-col bg-slate-900 items-center w-screen h-screen">
                <div className="">
                    <Image src="/logo.svg" height={180} width={250} />
                </div>
                <div className="flex flex-col gap-y-3 bg-zinc-50 min-w-[21rem] min-h-max rounded-md p-6">
                    <div className="">
                        <h3 className="font-bold text-gray-800 text-xl">
                            Crypto Converter
                        </h3>
                    </div>
                    <div className="flex flex-col gap-y-1">
                        <label className="font-semibold text-gray-500 text-base">
                            NEP
                        </label>
                        <input
                            type="number"
                            name="nep"
                            value={nep}
                            placeholder="0.00"
                            onChange={(e) => handleNep(e)}
                            className="border border-gray-300 rounded text-gray-600 py-1 px-2 focus:outline-none focus:border-2 focus:border-gray-400"
                        />
                    </div>
                    <div className="flex items-center justify-center text-gray-500">
                        <Image src="/sync.svg" height={30} width={50} />
                    </div>
                    <div className="flex flex-col gap-y-1">
                        <label className="font-semibold text-gray-500 text-base">
                            BUSD
                        </label>
                        <input
                            type="number"
                            name="busd"
                            value={busd}
                            placeholder="0.00"
                            onChange={(e) => handleBusd(e)}
                            className="border border-gray-300 rounded text-gray-600 py-1 px-2 focus:outline-none focus:border-2 focus:border-gray-400"
                        />
                    </div>
                    <div className="flex items-center justify-center w-full h-full pt-2">
                        <button
                            className="font-medium text-sm text-blue-700 opacity-80"
                            onClick={() => setShow(!show)}
                        >
                            Check Wallet Details
                        </button>
                    </div>
                </div>
            </div>
            {(show && !isConnected && !connecting) ? (
                <Disconnect
                    setShow={setShow}
                    connect={connect}
                    setIsConnecting={setIsConnecting}
                />
            ) : null}
            {(show && isConnected && !connecting) ? (
                <Connected
                    setShow={setShow}
                    balance={balance}
                    chainId={chainId}
                    setIsConnected={setIsConnected}
                    currentAccount={currentAccount}
                    setCurrentAccount={setCurrentAccount}
                />
            ) : null}
            {connecting && <Loading />}
        </>
    );
};

export default Home;
