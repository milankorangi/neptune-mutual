import React from "react";
import Image from "next/image";

interface ToggleProps {
    setShow: (show: boolean) => void;
    currentAccount: string;
    balance: any;
    chainId: any;
    setIsConnected: (value: boolean) => void;
    setCurrentAccount: (value: null) => void;
}

declare global {
    interface Window {
        ethereum: any;
        web3: any;
    }
}

function Connected({
    setShow,
    balance,
    currentAccount,
    chainId,
    setIsConnected,
    setCurrentAccount,
}: ToggleProps) {
    const handleDisconnet = async () => {
        setShow(false);
        setIsConnected(false);
        setCurrentAccount(null);
    };

    return (
        <div className="min-w-screen h-screen backdrop-brightness-10 animated fadeIn faster  fixed  left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover">
            <div className="absolute px-4 w-full max-w-md md:h-auto">
                <div className="relative bg-white rounded shadow dark:bg-gray-700 p-2.5 px-4">
                    <div className="flex justify-between items-center ">
                        <h3 className="text-xl font-medium text-gray-900 dark:text-white ">
                            Wallet Details
                        </h3>
                        <button
                            type="button"
                            className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                            data-modal-toggle="small-modal"
                            onClick={() => setShow(false)}
                        >
                            <svg
                                className="w-5 h-5"
                                fill="currentColor"
                                viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                    clipRule="evenodd"
                                ></path>
                            </svg>
                        </button>
                    </div>
                    <div className="flex flex-col p-4 gap-y-3">
                        <div className="flex items-center justify-between border-b-2 border-gray-300 pb-1">
                            <h4 className="font-semibold text-base text-gray-800">
                                KEY
                            </h4>
                            <h4 className="font-semibold text-base text-gray-800">
                                VALUE
                            </h4>
                        </div>
                        <div className="flex items-center justify-between border-b-2 border-gray-300 pb-1">
                            <h4 className="font-semibold text-sm text-gray-800">
                                Account
                            </h4>
                            <h4 className="font-semibold text-sm text-gray-800">
                                {currentAccount.replace(
                                    currentAccount.slice(4, -4),
                                    "***"
                                )}
                            </h4>
                        </div>
                        <div className="flex items-center justify-between border-b-2 border-gray-300 pb-1">
                            <h4 className="font-semibold text-sm text-gray-800">
                                Chain ID
                            </h4>
                            <h4 className="font-semibold text-sm text-gray-800">
                                {chainId}
                            </h4>
                        </div>
                        <div className="flex items-center justify-between border-b-2 border-gray-300 pb-1">
                            <h4 className="font-semibold text-sm text-gray-800">
                                Balance
                            </h4>
                            <div className="flex flex-row gap-x-2">
                                <Image
                                    src="/solona.svg"
                                    height={14}
                                    width={12}
                                />
                                <h4 className="font-semibold text-sm text-gray-800">
                                    {balance}
                                </h4>
                            </div>
                        </div>
                        <div className="flex items-center justify-center">
                            <h4 className="font-semibold text-sm text-gray-600">
                                Wallet Details
                            </h4>
                        </div>
                    </div>
                    <div className="flex items-center justify-center pb-2 space-x-2 ">
                        <button
                            type="button"
                            onClick={handleDisconnet}
                            className="flex-1 text-white bg-red-600 hover:bg-red-700 focus:ring-2 focus:ring-blue-300 font-medium rounded text-sm px-5 py-1.5 text-center"
                        >
                            Disconnect
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Connected;
