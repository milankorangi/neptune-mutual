import React from "react";

function Loading() {

    return (
        <div className="min-w-screen h-screen backdrop-brightness-10 animated fadeIn faster  fixed  left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover">
            <div className="absolute px-4 w-full max-w-md h-auto">
                <div className="flex justify-center items-center">
                    <div
                        className="spinner-border animate-spin inline-block w-8 h-8 border-4 rounded-full"
                        role="status"
                    >
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Loading;
