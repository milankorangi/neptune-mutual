import React from "react";

interface ToggleProps {
    setShow: (value: boolean) => void;
    connect: (provider: any) => void;
    setIsConnecting: (value: boolean) => void;
}
declare global {
    interface Window {
        ethereum: any;
        web3: any;
    }
}

function Disconnect({ setShow, connect, setIsConnecting }: ToggleProps) {
    const detectProvider = () => {
        let provider;
        if (window.ethereum) {
            provider = window.ethereum;
        } else if (window.web3) {
            provider = window.web3.currentProvider;
        } else {
            window.alert("No Ethereum browser detected! Check out MetaMask");
        }
        return provider;
    };

    const handleConnect = async () => {
        const provider = detectProvider();
        if (provider) {
            if (provider !== window.ethereum) {
                console.error(
                    "Not window.ethereum provider. Do you have multiple wallet installed ?"
                );
            }
            setIsConnecting(true);
            await provider.request({
                method: "wallet_requestPermissions",
                params: [
                    {
                        eth_accounts: {},
                    },
                ],
            });
        }
        connect(provider);
    };

    return (
        <div className="min-w-screen h-screen backdrop-brightness-10 animated fadeIn faster  fixed  left-0 top-0 flex justify-center items-center inset-0 z-50 outline-none focus:outline-none bg-no-repeat bg-center bg-cover">
            <div className="absolute px-4 w-full max-w-md h-auto">
                <div className="relative bg-white rounded shadow dark:bg-gray-700 p-2.5 px-4">
                    <div className="flex justify-between items-center ">
                        <h3 className="text-xl font-medium text-gray-900 dark:text-white ">
                            Wallet Details
                        </h3>
                        <button
                            type="button"
                            className="text-gray-400 bg-transparent hover:bg-gray-200 hover:text-gray-900 rounded-lg text-sm p-1 ml-auto inline-flex items-center dark:hover:bg-gray-600 dark:hover:text-white"
                            data-modal-toggle="small-modal"
                            onClick={() => setShow(false)}
                        >
                            <svg
                                className="w-5 h-5"
                                fill="currentColor"
                                viewBox="0 0 20 20"
                                xmlns="http://www.w3.org/2000/svg"
                            >
                                <path
                                    fillRule="evenodd"
                                    d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
                                    clipRule="evenodd"
                                ></path>
                            </svg>
                        </button>
                    </div>
                    <div className="py-4">
                        <p className="text-base font-medium leading-tight opacity-70 text-red-700">
                            Wallet not connected. Please click the &quot;Connect
                            Now&quot; button below.
                        </p>
                    </div>
                    <div className="flex items-center justify-center pb-2 space-x-2 ">
                        <button
                            type="button"
                            onClick={handleConnect}
                            className="flex-1 text-white bg-blue-700 hover:bg-blue-800 focus:ring-2 focus:ring-blue-300 font-medium rounded text-sm px-5 py-1.5 text-center"
                        >
                            Connect Now
                        </button>
                        <button
                            data-modal-toggle="small-modal"
                            type="button"
                            onClick={() => setShow(false)}
                            className="flex-1 text-gray-500 bg-gray-50 hover:bg-gray-100 focus:ring-2 focus:ring-gray-300 rounded border border-gray-200 text-sm font-medium px-5 py-1.5 hover:text-gray-900 focus:z-10 dark:bg-gray-700 dark:text-gray-300 dark:border-gray-500 dark:hover:text-white dark:hover:bg-gray-600"
                        >
                            Cancel
                        </button>
                    </div>
                </div>
            </div>
        </div>
    );
}

export default Disconnect;
